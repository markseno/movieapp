package com.example.movieapp.utils

import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.onStart
import com.example.movieapp.common.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map

fun <R> resultFlow(
    networkCall: suspend () -> Result<R>
): Flow<Result<R>> =
    flow {
        val responseStatus = networkCall.invoke()
        if (responseStatus.status == Result.Status.SUCCESS) {
            responseStatus.data?.let { emit(Result.success(it)) }
        } else if (responseStatus.status == Result.Status.ERROR) {
            if (responseStatus.message != null) {
                emit(Result.error(responseStatus.message))
            }
        }
    }.onStart {
        emit(Result.loading())
    }.catch {
        emit(Result.error(it.message.toString()))
    }.distinctUntilChanged()


fun <L> resultFlowWithRoom(
    databaseQuery: suspend () -> Flow<L>
): Flow<Result<L>> = flow {
    val source = databaseQuery.invoke()
        .onStart { emit(Result.loading<L>()) }
        .map {Result.success(it) }
        .flowOn(Dispatchers.IO)
        .catch { e ->
            emit(Result.error(e.message ?: "Unknown error"))
        }

    emitAll(source)
}



