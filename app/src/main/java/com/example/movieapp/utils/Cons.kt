package com.example.movieapp.utils

object Cons {
    const val ERROR_DIALOG = "errorDialog"
    const val LOADING_DIALOG = "loadingDialog"
    const val MESSAGE_DIALOG = "messageDialog"
    const val MOVIE_ID = "movieId"
    const val IS_POPULAR = "isPopular"
}