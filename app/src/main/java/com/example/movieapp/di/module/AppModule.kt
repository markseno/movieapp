package com.example.movieapp.di.module

import android.app.Application
import android.content.Context
import com.example.movieapp.MovieApp
import com.example.movieapp.data.local.MovieDatabase
import com.example.movieapp.data.remote.MoviesRemoteDataSource
import com.example.movieapp.data.remote.MoviesService
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(app: MovieApp): Context = app

    @Provides
    @Singleton
    fun provideApplication(app: MovieApp): Application = app

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): MoviesService =
        retrofit.create(MoviesService::class.java)

    @Provides
    @Singleton
    fun provideRoomDB(app: Application) = MovieDatabase.getInstance(app)

    @Provides
    @Singleton
    fun providePlayerDao(db: MovieDatabase) = db.movieDao()

    @Provides
    @Singleton
    @Named("IO")
    fun provideBackgroundDispatchers(): CoroutineDispatcher =
        Dispatchers.IO

    @Provides
    @Singleton
    @Named("MAIN")
    fun provideMainDispatchers(): CoroutineDispatcher =
        Dispatchers.Main

    @Provides
    @Singleton
    fun provideRemoteDataSource(apiService: MoviesService) = MoviesRemoteDataSource(apiService)
}