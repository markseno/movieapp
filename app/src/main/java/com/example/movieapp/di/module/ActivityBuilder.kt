package com.example.movieapp.di.module

import com.example.movieapp.ui.detail.DetailActivity
import com.example.movieapp.ui.main.MainActivity
import com.example.movieapp.ui.seeall.SeeAllActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributesDetailActivity(): DetailActivity

    @ContributesAndroidInjector
    abstract fun contributesSeeAllActivity(): SeeAllActivity

}