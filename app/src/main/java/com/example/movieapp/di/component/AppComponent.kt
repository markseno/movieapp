package com.example.movieapp.di.component

import com.example.movieapp.di.module.ActivityBuilder
import com.example.movieapp.di.module.AppModule
import com.example.movieapp.di.module.NetworkModule
import com.example.movieapp.di.module.ViewModelModule
import com.example.movieapp.MovieApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ActivityBuilder::class,
        AppModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        AndroidSupportInjectionModule::class
    ]
)

interface AppComponent : AndroidInjector<MovieApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: MovieApp): Builder

        fun build(): AppComponent
    }

}