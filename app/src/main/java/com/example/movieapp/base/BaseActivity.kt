package com.example.movieapp.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.example.movieapp.R
import com.example.movieapp.ui.dialog.DialogErrorFragment
import com.example.movieapp.ui.dialog.DialogLoadingFragment
import com.example.movieapp.utils.Cons.ERROR_DIALOG
import com.example.movieapp.utils.Cons.LOADING_DIALOG
import com.example.movieapp.utils.Cons.MESSAGE_DIALOG
import com.example.movieapp.utils.dismissDialogExt
import com.example.movieapp.utils.isDialogFragmentShowing
import com.example.movieapp.utils.showDialogExt
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity<B : ViewBinding, V : ViewModel> : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var mViewBinding: B
    lateinit var mViewModel: V

    val binding: B get() = mViewBinding
    val viewModel: V get() = mViewModel

    abstract fun injectViewModel()
    abstract fun getViewModelClass(): Class<V>
    abstract fun initView()
    abstract fun initListener()
    @LayoutRes
    abstract fun getLayoutResourceId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Base_Theme_MovieApp)
        super.onCreate(savedInstanceState)
        injectViewModel()
        mViewBinding = getViewBinding()
        setContentView(mViewBinding.root)
        initView()
        initListener()
    }

    abstract fun getViewBinding(): B


    fun showLoadingDialog() {
        if (!isDialogFragmentShowing(LOADING_DIALOG, supportFragmentManager)) {
            showDialogExt(DialogLoadingFragment(), LOADING_DIALOG, supportFragmentManager)
        }
    }

    fun dismissDialog(tag: String) {
        if (isDialogFragmentShowing(tag, supportFragmentManager)) {
            dismissDialogExt(tag, supportFragmentManager)
        }
    }

    fun showErrorDialog(message: String) {
        if (!isDialogFragmentShowing(ERROR_DIALOG, supportFragmentManager)) {
            dismissDialog(LOADING_DIALOG)
            val bundle = Bundle()
            bundle.putString(MESSAGE_DIALOG, message)
            showDialogExt(
                DialogErrorFragment().newInstance(bundle),
                ERROR_DIALOG, supportFragmentManager)

        }
    }

}