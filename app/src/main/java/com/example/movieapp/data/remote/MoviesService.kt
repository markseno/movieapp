package com.example.movieapp.data.remote

import com.example.movieapp.data.models.detailmovie.DetailMovie
import com.example.movieapp.data.models.genre.Genres
import com.example.movieapp.data.models.movies.Movies
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesService {

    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w300/"
        const val STARTING_PAGE_INDEX = 1
        const val SORT_TYPE = "popularity.desc"
    }

    @GET("genre/movie/list")
    suspend fun getGenreList(
        @Query("api_key") api_key: String?,
    ):  Response<Genres>

    @GET("movie/{movie_id}")
    suspend fun getDetailMovieById(
        @Path(value = "movie_id", encoded = true) movieId: Int,
        @Query("api_key") api_key: String?,
        @Query("append_to_response") append_to_response: String?
    ):  Response<DetailMovie>

    @GET("movie/upcoming")
    suspend fun getUpcomingMovie(
        @Query("api_key") api_key: String?,
        @Query("page") page: Int?
    ):  Response<Movies>

    @GET("discover/movie")
    suspend fun getPopularMovie(
        @Query("api_key") api_key: String?,
        @Query("sort") sort: String?,
        @Query("page") page: Int?
    ):  Response<Movies>

    @GET("search/movie")
    suspend fun searchPopularMovie(
        @Query("api_key") api_key: String?,
        @Query("query") query: String?,
        @Query("sort") sort: String?,
        @Query("page") page: Int?
    ):  Response<Movies>
}