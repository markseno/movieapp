package com.example.movieapp.data.models.movies

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(
    tableName = "favorite_movie"
)
@Parcelize
data class Movie(
    @PrimaryKey(autoGenerate = false) val id: Int,
    val title: String,
    val vote_average: String,
    val backdrop_path: String,
    val poster_path: String,
    val genre_ids: List<Int>
) : Parcelable