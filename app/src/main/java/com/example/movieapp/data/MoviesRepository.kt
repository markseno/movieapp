package com.example.movieapp.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.movieapp.data.local.dao.MovieDao
import com.example.movieapp.data.models.movies.Movie
import com.example.movieapp.data.remote.MoviesPagingRemoteDataSource
import com.example.movieapp.data.remote.MoviesRemoteDataSource
import com.example.movieapp.data.remote.MoviesService
import com.example.movieapp.utils.resultFlow
import com.example.movieapp.utils.resultFlowWithRoom
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class MoviesRepository @Inject constructor(
    private val dao: MovieDao,
    private val remote: MoviesRemoteDataSource,
    private val moviesService: MoviesService,
    @Named("IO") private val io: CoroutineDispatcher
) {

    fun getGenreList() = resultFlow { remote.getGenreList() }

    fun getUpcomingMovie() = resultFlow { remote.getUpcomingMovie() }

    fun getPopularMovie() = resultFlow { remote.getPopularMovie() }

    fun getDetailMovieById(movieId: Int) = resultFlow { remote.getDetailMovieById(movieId) }

    fun getPopularMoviePaging(
        isPopularMovie: Boolean,
        valueSearch: String
    ): Flow<PagingData<Movie>> {
        return Pager(
            config = PagingConfig(enablePlaceholders = false, pageSize = 20, initialLoadSize = 20),
            pagingSourceFactory = {
                MoviesPagingRemoteDataSource(
                    moviesService,
                    valueSearch,
                    dao,
                    isPopularMovie,
                    io)
            }
        ).flow
    }

    fun loadAllFavMovie() = resultFlowWithRoom { dao.loadAllFavMovie() }

    fun addFavMovie(movie: Movie) = dao.addFavMovie(movie)

    fun removeFavMovieById(movieId: Int) = dao.removeFavMovieById(movieId)

    fun getFavMovieById(movieId: Int) = dao.getFavMovieById(movieId)
}