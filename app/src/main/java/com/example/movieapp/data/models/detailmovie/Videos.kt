package com.example.movieapp.data.models.detailmovie

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Videos(
    val results: List<TrailerMovie>
) : Parcelable