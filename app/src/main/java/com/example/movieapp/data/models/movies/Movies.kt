package com.example.movieapp.data.models.movies

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movies(
    val results: List<Movie>,
    val page: Int,
    val total_pages: Int,
    val total_results: Int
) : Parcelable