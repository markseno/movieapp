package com.example.movieapp.data.remote

import com.example.movieapp.BuildConfig.TMDB_API_KEY
import com.example.movieapp.base.BaseDataSource
import com.example.movieapp.data.remote.MoviesService.Companion.SORT_TYPE
import com.example.movieapp.data.remote.MoviesService.Companion.STARTING_PAGE_INDEX
import javax.inject.Inject

class MoviesRemoteDataSource @Inject constructor(private val service: MoviesService) :
    BaseDataSource() {

    suspend fun getGenreList() = getResult {
        service.getGenreList(TMDB_API_KEY)
    }

    suspend fun getDetailMovieById(movieId: Int) = getResult {
        service.getDetailMovieById(
            movieId,
            TMDB_API_KEY,
            "videos,credits"
        )
    }

    suspend fun getUpcomingMovie() = getResult {
        service.getUpcomingMovie(
            TMDB_API_KEY,
            STARTING_PAGE_INDEX
        )
    }

    suspend fun getPopularMovie() = getResult {
        service.getPopularMovie(
            TMDB_API_KEY,
            SORT_TYPE,
            STARTING_PAGE_INDEX
        )
    }
}