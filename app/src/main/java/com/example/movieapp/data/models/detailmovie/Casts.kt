package com.example.movieapp.data.models.detailmovie

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Casts(
    val cast: List<Cast>
) : Parcelable