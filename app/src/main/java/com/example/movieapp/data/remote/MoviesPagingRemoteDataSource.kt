package com.example.movieapp.data.remote

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.movieapp.BuildConfig.TMDB_API_KEY
import com.example.movieapp.data.local.dao.MovieDao
import com.example.movieapp.data.models.movies.Movie
import com.example.movieapp.data.remote.MoviesService.Companion.SORT_TYPE
import com.example.movieapp.data.remote.MoviesService.Companion.STARTING_PAGE_INDEX
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class MoviesPagingRemoteDataSource(
    private val moviesService: MoviesService,
    private val valueSearch: String,
    private val dao: MovieDao,
    private val isPopularMovie: Boolean,
    private val io: CoroutineDispatcher
) :
    PagingSource<Int, Movie>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {
        return try {
            if (isPopularMovie) {
                val page = params.key ?: STARTING_PAGE_INDEX
                val response = if (valueSearch.isEmpty()) moviesService.getPopularMovie(TMDB_API_KEY, SORT_TYPE, page) else moviesService.searchPopularMovie(TMDB_API_KEY, valueSearch,"popularity.desc", page)
                val movies = response.body()?.results?: emptyList()
                LoadResult.Page(
                    data = movies,
                    prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1,
                    nextKey = if (page >= (response.body()?.total_pages ?: 0)) null else page + 1
                )
            } else {
                val page = params.key ?: 0
                val entities = withContext(io) { dao.searchFavMoviesByTitle(valueSearch, params.loadSize, page * params.loadSize) }
                delay(500)
                LoadResult.Page(
                    data = entities,
                    prevKey = if (page == 0) null else page - 1,
                    nextKey = if (entities.isEmpty()) null else page + 1
                )
            }
        } catch (exception: IOException) {
            val error = IOException("Please Check Internet Connection")
            LoadResult.Error(error)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }

    }

    override fun getRefreshKey(state: PagingState<Int, Movie>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }
}