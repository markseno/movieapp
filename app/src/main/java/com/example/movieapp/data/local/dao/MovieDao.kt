package com.example.movieapp.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.movieapp.data.models.movies.Movie
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDao {

    @Query("DELETE FROM favorite_movie WHERE id = :movieId")
    fun removeFavMovieById(movieId: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addFavMovie(movie: Movie)

    @Query("SELECT * FROM favorite_movie WHERE id = :movieId")
    fun getFavMovieById(movieId: Int): Movie?

    @Query("SELECT * FROM favorite_movie LIMIT 10")
    fun loadAllFavMovie(): Flow<List<Movie>>

    @Query("SELECT * FROM favorite_movie WHERE title LIKE '%' || :title || '%' LIMIT :limit OFFSET :offset")
    fun searchFavMoviesByTitle(title: String, limit: Int, offset: Int): List<Movie>
}