package com.example.movieapp.data.models.detailmovie

import android.os.Parcelable
import com.example.movieapp.data.models.genre.Genre
import kotlinx.parcelize.Parcelize

@Parcelize
data class DetailMovie(
    val id: Int,
    val title: String,
    val vote_average: Double,
    val overview: String,
    val release_date: String,
    val vote_count: Int,
    val videos: Videos,
    val genres: List<Genre>,
    val credits: Casts,
    val backdrop_path: String,
    val poster_path: String,
) : Parcelable