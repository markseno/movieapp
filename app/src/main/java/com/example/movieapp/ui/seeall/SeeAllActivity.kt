package com.example.movieapp.ui.seeall

import android.content.Intent
import android.os.Handler
import android.os.Looper
import androidx.core.widget.addTextChangedListener
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.example.movieapp.R
import com.example.movieapp.base.BaseActivity
import com.example.movieapp.common.Result
import com.example.movieapp.data.models.genre.Genre
import com.example.movieapp.databinding.ActivitySeeAllBinding
import com.example.movieapp.di.injectViewModel
import com.example.movieapp.ui.detail.DetailActivity
import com.example.movieapp.ui.dialog.DialogErrorFragment
import com.example.movieapp.utils.Cons
import com.example.movieapp.utils.Cons.IS_POPULAR
import com.example.movieapp.utils.Cons.LOADING_DIALOG
import com.example.movieapp.utils.Cons.MOVIE_ID
import timber.log.Timber

class SeeAllActivity: BaseActivity<ActivitySeeAllBinding, SeeAllViewModel>(),
    DialogErrorFragment.DialogErrorFragmentListener{

    private lateinit var seeAllAdapter : SeeAllAdapter
    private var isPopular = false

    override fun injectViewModel() { mViewModel = injectViewModel(viewModelFactory) }

    override fun getViewModelClass(): Class<SeeAllViewModel> = SeeAllViewModel::class.java

    override fun getLayoutResourceId(): Int = R.layout.activity_see_all

    override fun getViewBinding(): ActivitySeeAllBinding { return ActivitySeeAllBinding.inflate(layoutInflater) }

    override fun initView() {
        isPopular = intent.getBooleanExtra(IS_POPULAR, false)
        binding.layoutHeader.tvTitle.text = if (isPopular) "Popular" else "Favorites"
    }

    override fun initListener() {
        binding.layoutHeader.ivBack.setOnClickListener {
            finish()
        }

        binding.etSearch.addTextChangedListener {
            Handler(Looper.getMainLooper()).postDelayed({
                fetchAllMovies(it.toString())
            }, 1500)
        }
    }

    private fun observeGenreList() {
        viewModel.fetchAllGenre().observe(this) { result ->
            when (result.status) {

                Result.Status.SUCCESS -> {
                    if (result.data != null) {
                        setupAdapter(result.data.genres)
                    } else {
                        showErrorDialog(result.message?: "Data Genre Kosong")
                    }
                }

                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }

                Result.Status.LOADING -> {
                }
            }
        }
    }

    private fun setupAdapter(genreList: List<Genre>) {
        seeAllAdapter = SeeAllAdapter(
            { goToDetailMovie(it) },
            genreList)

        binding.rvSeeAll.apply {
            layoutManager = GridLayoutManager(this@SeeAllActivity, 2)
            setHasFixedSize(true)
            adapter = seeAllAdapter.withLoadStateFooter(
                footer = SeeAllLoadingStateAdapter { seeAllAdapter.retry() }
            )
        }

        seeAllAdapter.addLoadStateListener { loadState ->
            if (loadState.refresh is LoadState.Loading ||
                loadState.append is LoadState.Loading)
                Timber.tag("seeAllAdapter: Loading")
            else {
                dismissDialog(LOADING_DIALOG)
                val errorState = when {
                    loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                    loadState.prepend is LoadState.Error ->  loadState.prepend as LoadState.Error
                    loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                    else -> null
                }
                errorState?.let {
                    if (seeAllAdapter.itemCount == 0) showErrorDialog(it.error.message?:"Unknown Error")
                }
            }
        }

        fetchAllMovies(binding.etSearch.text.toString())
    }

    private fun fetchAllMovies(valueSearch: String) {
        viewModel.fetchAllMovies(isPopular, valueSearch).observe(this) {
            seeAllAdapter.submitData(this@SeeAllActivity.lifecycle, it)
        }
    }

    private fun goToDetailMovie(movieId: Int){
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(MOVIE_ID,movieId)
        startActivity(intent)
    }

    override fun onDialogErrorFragmentListener() {
        dismissDialog(Cons.ERROR_DIALOG)
        showLoadingDialog()
        observeGenreList()
    }

    override fun onResume() {
        super.onResume()
        observeGenreList()
    }
}