package com.example.movieapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import javax.inject.Inject
import com.example.movieapp.data.MoviesRepository
import com.example.movieapp.data.models.genre.Genres
import com.example.movieapp.data.models.movies.Movies
import com.example.movieapp.common.Result
import com.example.movieapp.data.models.movies.Movie
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Named

class MainViewModel @Inject constructor(
    private val repository: MoviesRepository,
    @Named("IO") private val io: CoroutineDispatcher
) : ViewModel() {

    fun fetchAllGenre(): LiveData<Result<Genres>> {
        val genres = MutableLiveData<Result<Genres>>()
        viewModelScope.launch {
            repository.getGenreList()
                .flowOn(io)
                .collectLatest {
                    genres.value = it
                }
        }
        return genres
    }

    fun fetchUpcomingMovies(): LiveData<Result<Movies>> {
        val upcomingMovies = MutableLiveData<Result<Movies>>()
        viewModelScope.launch {
            repository.getUpcomingMovie()
                .flowOn(io)
                .collectLatest {
                    upcomingMovies.value = it
                }
        }
        return upcomingMovies
    }

    fun fetchFavoriteMovieFromLocal(): LiveData<Result<List<Movie>>> {
        val favoriteMovies = MutableLiveData<Result<List<Movie>>>()
        viewModelScope.launch {
            repository.loadAllFavMovie()
                .flowOn(io)
                .collectLatest {
                    favoriteMovies.value = it
                }
        }
        return favoriteMovies
    }

    fun fetchPopularMovie(): LiveData<Result<Movies>> {
        val popularMovies = MutableLiveData<Result<Movies>>()
        viewModelScope.launch {
            repository.getPopularMovie()
                .flowOn(io)
                .collectLatest {
                    popularMovies.value = it
                }
        }
        return popularMovies
    }

}