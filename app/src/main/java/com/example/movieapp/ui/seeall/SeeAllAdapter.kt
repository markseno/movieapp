package com.example.movieapp.ui.seeall

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieapp.R
import com.example.movieapp.data.models.genre.Genre
import com.example.movieapp.data.models.movies.Movie
import com.example.movieapp.data.remote.MoviesService
import com.example.movieapp.databinding.ItemPopularBinding

class SeeAllAdapter(private val clicked: (Int) -> Unit, private val genreList: List<Genre>) :
    PagingDataAdapter<Movie, SeeAllAdapter.MoviesViewHolder> (
        MoviesDiffCallback()
    ) {

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {

        val data = getItem(position)

        holder.bind(data)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {

        return MoviesViewHolder(
            ItemPopularBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    }

    inner class MoviesViewHolder(private var binding: ItemPopularBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Movie?) = binding.apply {

            root.setOnClickListener {
                if (item != null) {
                    clicked.invoke(item.id)
                }
            }

            Glide.with(root.context)
                .load(MoviesService.BASE_URL_IMAGE.plus(item?.poster_path))
                .error(ContextCompat.getDrawable(itemView.context, R.color.greyLight))
                .into(ivMoviePoster)

            tvMovieName.text = item?.title ?: "-"
            tvMovieRate.text = item?.vote_average ?: "-"
            tvMovieGenre.text = prepareShowGenre(item?.genre_ids ?: emptyList())

        }
    }

    private class MoviesDiffCallback : DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem == newItem
        }
    }

    private fun prepareShowGenre(list : List<Int>) : String {
        val listGenre : MutableList<String> = arrayListOf()
        list.forEach { id ->
            genreList.forEach { genre ->
                if (id == genre.id){
                    listGenre.add(genre.name)
                }
            }
        }

        return listGenre.toString().replace("[", "").replace("]", "")
    }
}