package com.example.movieapp.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.movieapp.data.MoviesRepository
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.example.movieapp.common.Result
import com.example.movieapp.data.models.detailmovie.DetailMovie
import com.example.movieapp.data.models.movies.Movie
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import javax.inject.Named

class DetailViewModel @Inject constructor(
    private val repository: MoviesRepository,
    @Named("IO") private val io: CoroutineDispatcher
) : ViewModel() {


    fun getDetailMovieById(movieId: Int): LiveData<Result<DetailMovie>> {
        val detailMovie = MutableLiveData<Result<DetailMovie>>()
        viewModelScope.launch {
            repository.getDetailMovieById(movieId)
                .flowOn(io)
                .collect { result ->
                    detailMovie.value = result
                }
        }
        return detailMovie
    }

    fun addFavMovie(movie: Movie) {
        viewModelScope.launch {
            withContext(io) {
                repository.addFavMovie(movie)
            }
        }
    }

    fun removeFavMovieById(movieId: Int) {
        viewModelScope.launch {
            withContext(io) {
                repository.removeFavMovieById(movieId)
            }
        }
    }

    suspend fun isFavMovie(movieId: Int): Boolean {
        return withContext(io) {
            repository.getFavMovieById(movieId) != null
        }
    }
}