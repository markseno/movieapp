package com.example.movieapp.ui.detail

import android.annotation.SuppressLint
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.example.movieapp.R
import com.example.movieapp.base.BaseActivity
import com.example.movieapp.data.models.detailmovie.Cast
import com.example.movieapp.databinding.ActivityDetailBinding
import com.example.movieapp.ui.dialog.DialogErrorFragment
import com.example.movieapp.di.injectViewModel
import com.example.movieapp.common.Result
import com.example.movieapp.data.models.detailmovie.DetailMovie
import com.example.movieapp.data.models.detailmovie.TrailerMovie
import com.example.movieapp.data.models.genre.Genre
import com.example.movieapp.data.models.movies.Movie
import com.example.movieapp.utils.Cons.ERROR_DIALOG
import com.example.movieapp.utils.Cons.LOADING_DIALOG
import com.example.movieapp.utils.Cons.MOVIE_ID
import com.example.movieapp.utils.convertStringIntoDateTime
import com.example.movieapp.utils.formatIntoK
import com.example.movieapp.utils.formatRate
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import kotlinx.coroutines.launch

class DetailActivity : BaseActivity<ActivityDetailBinding, DetailViewModel>(),
    DialogErrorFragment.DialogErrorFragmentListener {

    private var movieId = 0

    private lateinit var detailMovie: DetailMovie

    override fun injectViewModel() { mViewModel = injectViewModel(viewModelFactory) }

    override fun getViewModelClass(): Class<DetailViewModel> = DetailViewModel::class.java

    override fun getLayoutResourceId(): Int = R.layout.activity_detail

    override fun getViewBinding(): ActivityDetailBinding { return ActivityDetailBinding.inflate(layoutInflater) }

    override fun initView() {
        showLoadingDialog()
        movieId = intent.getIntExtra(MOVIE_ID, 0)
        getDetailMovie(movieId)
        binding.layoutHeader.tvTitle.text = this.getString(R.string.movie)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun initListener() {
        binding.layoutHeader.ivBack.setOnClickListener {
            finish()
        }

        binding.ivAddFavorites.setOnClickListener {
            lifecycleScope.launch {
                setFavIcon(viewModel.isFavMovie(movieId), true)
            }
        }
    }

    private fun setupCastAdapter(list: List<Cast>) {
        val castMovieAdapter = CastMovieAdapter(list)

        binding.rvCast.layoutManager =
            LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL,
                false
            )

        binding.rvCast.adapter = castMovieAdapter
    }

    private fun getDetailMovie(movieId: Int) {
        viewModel.getDetailMovieById(movieId).observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    if (result.data != null) {
                        binding.scrollView.visibility = View.VISIBLE
                        dismissDialog(LOADING_DIALOG)
                        detailMovie = result.data
                        prepareSpreadData(result.data)
                        setupCastAdapter(result.data.credits.cast)
                        lifecycleScope.launch {
                            setFavIcon(viewModel.isFavMovie(movieId), false)
                        }
                    } else {
                        showErrorDialog(result.message?: "Detail Movie Tidak Ditemukan")
                    }
                }

                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }

                Result.Status.LOADING -> {
                }
            }
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun setFavIcon(isFavMovie: Boolean, withAction: Boolean) {
        if (isFavMovie) {
            if (withAction) {
                viewModel.removeFavMovieById(movieId)
                binding.ivAddFavorites.setImageDrawable(this.getDrawable(R.drawable.ic_fav_not_click))
            } else {
                binding.ivAddFavorites.setImageDrawable(this.getDrawable(R.drawable.ic_fav_clicked))
            }
        } else {
            if (withAction) {
                viewModel.addFavMovie(
                    Movie (
                        detailMovie.id,
                        detailMovie.title,
                        formatRate(detailMovie.vote_average),
                        detailMovie.backdrop_path,
                        detailMovie.poster_path,
                        convertToListInt(detailMovie.genres)
                    )
                )
                binding.ivAddFavorites.setImageDrawable(this.getDrawable(R.drawable.ic_fav_clicked))
            } else {
                binding.ivAddFavorites.setImageDrawable(this.getDrawable(R.drawable.ic_fav_not_click))
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun prepareSpreadData(detailMovie: DetailMovie) {
        binding.tvTitleMovie.text = detailMovie.title
        binding.tvMovieRate.text = formatRate(detailMovie.vote_average)
        binding.tvGenreMovie.text = prepareShowGenre(detailMovie.genres)
        binding.tvOverview.text = detailMovie.overview
        binding.tvTotalVotes.text = formatIntoK(detailMovie.vote_count) + " Votes"
        binding.tvReleaseYear.text = detailMovie.release_date.substring(0,4)

        binding.youtubePlayerVideo.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                binding.loadingYoutube.visibility = View.GONE
                binding.youtubePlayerVideo.visibility = View.VISIBLE
                youTubePlayer.cueVideo(getMovieTrailerId(detailMovie.videos.results), 0f)
            }

            override fun onError(youTubePlayer: YouTubePlayer, error: PlayerConstants.PlayerError) {
                super.onError(youTubePlayer, error)
                binding.loadingYoutube.visibility = View.GONE
                binding.youtubePlayerVideo.visibility = View.VISIBLE
            }
        })
    }

    private fun prepareShowGenre(
        list : List<Genre>
    ) : String {
        val listGenre : MutableList<String> = arrayListOf()
        list.forEach {
            listGenre.add(it.name)
        }

        return listGenre.toString().replace("[","").replace("]","")
    }

    private fun convertToListInt(
        list : List<Genre>
    ) : List<Int> {
        val listGenre : ArrayList<Int> = arrayListOf()
        list.forEach {
            listGenre.add(it.id)
        }
        return listGenre
    }

    private fun getMovieTrailerId(
        trailerMovies: List<TrailerMovie>
    ): String {
        var trailerMovieId = ""

        trailerMovies.sortedByDescending {
            convertStringIntoDateTime(it.published_at.substring(0,10))
        }

        trailerMovies.forEach {
            if (it.official){
                if (it.site.lowercase() == "youtube"){
                    if (it.type.lowercase() == "trailer"){
                        if (trailerMovieId.isEmpty()){
                            trailerMovieId = it.key
                        }
                    }
                }
            }
        }
        return trailerMovieId
    }

    override fun onDialogErrorFragmentListener() {
        dismissDialog(ERROR_DIALOG)
        showLoadingDialog()
        getDetailMovie(movieId)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.youtubePlayerVideo.release()
    }
}