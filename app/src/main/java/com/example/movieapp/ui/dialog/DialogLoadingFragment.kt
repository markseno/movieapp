package com.example.movieapp.ui.dialog

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.movieapp.base.BaseDialog
import com.example.movieapp.databinding.DialogLoadingBinding

class DialogLoadingFragment : BaseDialog<DialogLoadingBinding>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> DialogLoadingBinding
        get() = DialogLoadingBinding::inflate

    override fun bindView() {
        isCancelable = false
    }

    override fun assignListener() {
    }
}




