package com.example.movieapp.ui.main

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import com.example.movieapp.R
import com.example.movieapp.base.BaseActivity
import com.example.movieapp.common.Result
import com.example.movieapp.data.models.genre.Genre
import com.example.movieapp.data.models.movies.Movie
import com.example.movieapp.databinding.ActivityMainBinding
import com.example.movieapp.di.injectViewModel
import com.example.movieapp.ui.detail.DetailActivity
import com.example.movieapp.ui.dialog.DialogErrorFragment
import com.example.movieapp.ui.seeall.SeeAllActivity
import com.example.movieapp.utils.Cons.ERROR_DIALOG
import com.example.movieapp.utils.Cons.IS_POPULAR
import com.example.movieapp.utils.Cons.LOADING_DIALOG
import com.example.movieapp.utils.Cons.MOVIE_ID
import www.sanju.zoomrecyclerlayout.ZoomRecyclerLayout

class MainActivity: BaseActivity<ActivityMainBinding, MainViewModel>(),
    DialogErrorFragment.DialogErrorFragmentListener {

    private var genreList: List<Genre> = arrayListOf()

    override fun injectViewModel() { mViewModel = injectViewModel(viewModelFactory) }

    override fun getViewModelClass(): Class<MainViewModel> = MainViewModel::class.java

    override fun getLayoutResourceId(): Int = R.layout.activity_main

    override fun getViewBinding(): ActivityMainBinding { return ActivityMainBinding.inflate(layoutInflater) }

    override fun initView() {
        collectDataGenre()
    }

    override fun initListener() {
        binding.tvSeeAllPopular.setOnClickListener {
            val intent = Intent(this, SeeAllActivity::class.java)
            intent.putExtra(IS_POPULAR, true)
            startActivity(intent)
        }

        binding.tvSeeAllFavorite.setOnClickListener {
            val intent = Intent(this, SeeAllActivity::class.java)
            intent.putExtra(IS_POPULAR, false)
            startActivity(intent)
        }
    }

    private fun setupAdapterUpComing(list: List<Movie>) {
        binding.rvUpcoming.apply {
            adapter = UpComingAdapter(list)
            layoutManager = ZoomRecyclerLayout(
                this@MainActivity, LinearLayoutManager.HORIZONTAL,
                false,
            )
            isNestedScrollingEnabled = false
            onFlingListener = null
        }

        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(binding.rvUpcoming)
        binding.rvUpcoming.post { binding.rvUpcoming.smoothScrollToPosition(4) }
    }

    private fun setupAdapterFavorite(list: List<Movie>, genreList: List<Genre>) {
        binding.rvFavorite.apply {
            adapter = FavoriteAdapter(list, genreList) { goToDetailMovie(it) }
            layoutManager = LinearLayoutManager(
                this@MainActivity, LinearLayoutManager.HORIZONTAL,
                false
            )
        }
    }

    private fun setupAdapterPopular(list: List<Movie>, genreList: List<Genre>) {
        binding.rvPopular.apply {
            adapter = PopularAdapter(list, genreList) { goToDetailMovie(it) }
            layoutManager =  object : GridLayoutManager(this@MainActivity, 2) {
                override fun canScrollVertically(): Boolean = false
            }
        }
    }

    private fun collectDataGenre() {
        viewModel.fetchAllGenre().observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    if (result.data != null) {
                        genreList = result.data.genres
                        collectDataAll()
                    } else {
                        showErrorDialog("Data Genre Kosong")
                    }
                }

                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }

                Result.Status.LOADING -> {
                }
            }
        }
    }

    private fun collectDataAll() {
        viewModel.fetchUpcomingMovies().observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    if (result.data != null) {
                        setupAdapterUpComing(result.data.results)

                        binding.loadingUpcoming.visibility = View.GONE
                        binding.rvUpcoming.visibility = View.VISIBLE
                    } else {
                        showErrorDialog("Data UpComing Kosong")
                    }
                }
                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }
                Result.Status.LOADING -> {
                }
            }
        }

        viewModel.fetchFavoriteMovieFromLocal().observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    if (result.data != null) {
                        if (result.data.isEmpty()) {
                            binding.loadingFavorite.visibility = View.GONE
                            binding.tvFavoriteMovieEmpty.visibility = View.VISIBLE
                            binding.rvFavorite.visibility = View.GONE
                        } else {
                            setupAdapterFavorite(result.data, genreList)

                            binding.loadingFavorite.visibility = View.GONE
                            binding.tvFavoriteMovieEmpty.visibility = View.GONE
                            binding.rvFavorite.visibility = View.VISIBLE
                            binding.tvSeeAllFavorite.visibility = View.VISIBLE
                        }
                    } else {
                        binding.loadingFavorite.visibility = View.GONE
                        binding.tvFavoriteMovieEmpty.visibility = View.VISIBLE
                    }
                }

                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }

                Result.Status.LOADING -> {
                }
            }
        }

        viewModel.fetchPopularMovie().observe(this) { result ->
            when (result.status) {
                Result.Status.SUCCESS -> {
                    dismissDialog(LOADING_DIALOG)
                    if (result.data != null) {
                        setupAdapterPopular(result.data.results, genreList)

                        binding.loadingPopular.visibility = View.GONE
                        binding.rvPopular.visibility = View.VISIBLE
                        binding.tvSeeAllPopular.visibility = View.VISIBLE
                    } else {
                        showErrorDialog("Data Popular Movie Kosong")
                    }
                }

                Result.Status.ERROR -> {
                    showErrorDialog(result.message?: "Sedang terjadi kendala")
                }

                Result.Status.LOADING -> {
                }
            }
        }
    }

    private fun goToDetailMovie(movieId: Int){
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(MOVIE_ID,movieId)
        startActivity(intent)
    }

    override fun onDialogErrorFragmentListener() {
        dismissDialog(ERROR_DIALOG)
        collectDataGenre()
    }
}