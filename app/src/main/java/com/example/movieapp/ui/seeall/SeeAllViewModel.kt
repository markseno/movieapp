package com.example.movieapp.ui.seeall

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.example.movieapp.common.Result
import javax.inject.Inject
import com.example.movieapp.data.MoviesRepository
import com.example.movieapp.data.models.genre.Genres
import com.example.movieapp.data.models.movies.Movie
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Named

class SeeAllViewModel @Inject constructor(
    private val repository: MoviesRepository,
    @Named("IO") private val io: CoroutineDispatcher
) : ViewModel() {

    fun fetchAllGenre(): LiveData<Result<Genres>> {
        val genres = MutableLiveData<Result<Genres>>()
        viewModelScope.launch {
            repository.getGenreList()
                .flowOn(io)
                .collectLatest {
                    genres.value = it
                }
        }
        return genres
    }

    fun fetchAllMovies(isPopular: Boolean, valueSearch: String): LiveData<PagingData<Movie>> {
        val allMovies = MutableLiveData<PagingData<Movie>>()
        viewModelScope.launch {
            repository.getPopularMoviePaging(isPopular, valueSearch)
                .flowOn(io)
                .collectLatest {
                    allMovies.value = it
                }
        }
        return allMovies
    }

}