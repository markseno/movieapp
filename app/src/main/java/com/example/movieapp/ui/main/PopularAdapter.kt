package com.example.movieapp.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieapp.R
import com.example.movieapp.data.models.genre.Genre
import com.example.movieapp.data.models.movies.Movie
import com.example.movieapp.data.remote.MoviesService.Companion.BASE_URL_IMAGE
import com.example.movieapp.databinding.ItemPopularBinding

class PopularAdapter(private val list: List<Movie>, private val genreList: List<Genre>, private val onClick: (Int) -> Unit) : RecyclerView.Adapter<PopularAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            ItemPopularBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    }

    override fun getItemCount(): Int = if (list.size >= 6) 6 else list.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position])
    }

    inner class Holder(private var binding: ItemPopularBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Movie?) = binding.apply {
            Glide.with(root.context)
                .load(BASE_URL_IMAGE.plus(item?.poster_path))
                .error(ContextCompat.getDrawable(itemView.context, R.color.greyLight))
                .into(ivMoviePoster)

            tvMovieName.text = item?.title ?: "-"
            tvMovieRate.text = item?.vote_average ?: "-"
            tvMovieGenre.text = prepareShowGenre(item?.genre_ids ?: emptyList())

            cvMain.setOnClickListener {
                onClick(item!!.id)
            }
        }
    }

    private fun prepareShowGenre(list: List<Int>): String {
        val listGenre: MutableList<String> = arrayListOf()
        list.forEach { id ->
            genreList.forEach { genre ->
                if (id == genre.id) {
                    listGenre.add(genre.name)
                }
            }
        }

        return listGenre.toString().replace("[", "").replace("]", "")
    }
}