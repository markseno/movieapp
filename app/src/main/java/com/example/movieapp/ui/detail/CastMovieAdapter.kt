package com.example.movieapp.ui.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movieapp.R
import com.example.movieapp.data.models.detailmovie.Cast
import com.example.movieapp.data.remote.MoviesService.Companion.BASE_URL_IMAGE
import com.example.movieapp.databinding.ItemCastMovieBinding

class CastMovieAdapter(val list: List<Cast>) : RecyclerView.Adapter<CastMovieAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            ItemCastMovieBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(list[position])
    }

    inner class Holder(private var binding: ItemCastMovieBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Cast?) = binding.apply {
            Glide.with(root.context)
                .load(BASE_URL_IMAGE.plus(item?.profile_path))
                .error(ContextCompat.getDrawable(itemView.context, R.drawable.rounded_grey))
                .circleCrop()
                .into(ivCast)

            tvCastName.text = item?.name ?: "-"
            tvDepartement.text = item?.known_for_department ?: "-"

        }
    }

}